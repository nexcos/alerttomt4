﻿#property strict
#include <stdlib.mqh>
#include <Arrays/ArrayObj.mqh>
#include <socket-library-mt4-mt5.mqh>

input int Magic = 1;
input int Slippage = 10;
input double Lot = 0.1;
input string AlertName = "test";
input string Host = "localhost";
input ushort Port = 45580;

enum
{
   OrderUnknown,
   OrderLong,
   OrderShort,
   OrderLongClose,
   OrderShortClose
};

enum
{
   ErrorFatal,
   ErrorTurnNext,
   ErrorPauseTurnNext,
   ErrorRetry
};

class Order : public CObject
{
private:
   int order_;
   
public:
   Order(int order) : order_(order) {}
   int GetOrder() { return order_; }
};

class Timer
{
private:
   long timer_;
   long time_stamp_;
   
public:
   void Set(long timer)
   {
      timer_ = timer;
      time_stamp_ = GetTickCount();
   }
   bool Update()
   {
      if (Playing())
      {
         long current = GetTickCount();
         long progress = current - time_stamp_;
         if (current < time_stamp_)
         {
            progress = 0xFFFFFFFF - time_stamp_ + current;
         }
         timer_ -= progress;
         time_stamp_ = current;
         return Playing();
      }
      return false;
   }
   bool Playing()
   {
      return timer_ > 0;
   }
};

ClientSocket* Socket = NULL;
CArrayObj OrderQueue;
Timer WaitTimer;
int LastError = 0;
const int OrderCountMax = 10;
const int TimerInterval = 200;
const int TurnNextInterval = 1000;
const int PauseTurnNextInterval = 60000;

void LogError(string function, string message, int error)
{
   PrintFormat("ERROR: in %s() %s", function, message);
   if (error != ERR_NO_ERROR)
   {
      PrintFormat("ERROR: code %d - %s", error, ErrorDescription(error));
   }
}

void ErrorHandling(string function, string message)
{
   int error = GetLastError();
   LogError(function, message, error);
   switch (error)
   {
      case ERR_INVALID_TRADE_PARAMETERS:     // 3.無効な取引パラメータ
      case ERR_OLD_VERSION:                  // 5.クライアントのバージョンが古い
      case ERR_NOT_ENOUGH_RIGHTS:            // 7.権限が無い
      case ERR_MALFUNCTIONAL_TRADE:          // 9.不適合な関数によってトレードが行われた
      case ERR_ACCOUNT_DISABLED:             // 64.アカウント無効化
      case ERR_INVALID_ACCOUNT:              // 65.無効なアカウント
      case ERR_INVALID_PRICE:                // 129.無効な価格値
      case ERR_INVALID_STOPS:                // 130.無効なストップ値
      case ERR_INVALID_TRADE_VOLUME:         // 131.無効なロット数
      case ERR_NOT_ENOUGH_MONEY:             // 134.資金不足
      case ERR_LONG_POSITIONS_ONLY_ALLOWED:  // 140.買いポジションだけ有効
      case ERR_TRADE_EXPIRATION_DENIED:      // 147.注文有効期限の設定が不正
      case ERR_TRADE_TOO_MANY_ORDERS:        // 148.オーダー数が限度を超えている
      default:
         LastError = ErrorFatal;
         break;
      case ERR_SERVER_BUSY:                  // 4.トレードサーバーがビジー状態
      case ERR_NO_CONNECTION:                // 6.トレードサーバと接続できない
      case ERR_TOO_FREQUENT_REQUESTS:        // 8.要求が多すぎる
      case ERR_TRADE_TIMEOUT:                // 128.トレード時間切れ
      case ERR_TRADE_DISABLED:               // 133.トレード無効化
      case ERR_PRICE_CHANGED:                // 135.価格値変更
      case ERR_OFF_QUOTES:                   // 136.相場価格から離れている
      case ERR_BROKER_BUSY:                  // 137.ブローカーがビジー状態
      case ERR_REQUOTE:                      // 138.再見積り
      case ERR_ORDER_LOCKED:                 // 139.注文がロックされた
      case ERR_TOO_MANY_REQUESTS:            // 141.要求が多すぎる
      case ERR_TRADE_MODIFY_DENIED:          // 145.市場が閉じている為、変更できない
      case ERR_TRADE_CONTEXT_BUSY:           // 146.トレード状況がビジー状態
         LastError = ErrorTurnNext;
         break;
      case ERR_MARKET_CLOSED:                // 132.市場が閉じている
         LastError = ErrorPauseTurnNext;
         break;
      case ERR_NO_ERROR:                     // 0.エラーなし
      case ERR_NO_RESULT:                    // 1.エラーは無いが、結果は未知
      case ERR_COMMON_ERROR:                 // 2.共通エラー
         LastError = ErrorRetry;
         break;
   }
}

int StrToOrderType(string src)
{
   if (src == "long")
   {
      return OrderLong;
   }
   if (src == "short")
   {
      return OrderShort;
   }
   if (src == "longclose")
   {
      return OrderLongClose;
   }
   if (src == "shortclose")
   {
      return OrderShortClose;
   }
   return OrderUnknown;
}

int AdjustSlippage(string symbol, int pips)
{
   int result = 0;
   int digits = (int)MarketInfo(symbol, MODE_DIGITS);
   if (digits == 2 || digits == 4)
   {
      result = pips;
   }
   else if (digits == 3 || digits == 5)
   {
      result = pips * 10;
   }
   return result;
}

bool OpenOrder(bool buy)
{
   int ticket;
   int slippage = AdjustSlippage(Symbol(), Slippage);
   do
   {
      if (buy)
      {
         ticket = OrderSend(Symbol(), OP_BUY, Lot, Ask, slippage, 0, 0, AlertName, Magic);
      }
      else
      {
         ticket = OrderSend(Symbol(), OP_SELL, Lot, Bid, slippage, 0, 0, AlertName, Magic);
      }
      if (ticket == -1)
      {
         ErrorHandling("OpenOrder", "Could not open order");
         if (LastError != ErrorRetry)
         {
            return false;
         }
      }
   } while (ticket == -1);
   return true;
}

bool CloseOrder(bool buy)
{
   int order_type = buy ? OP_BUY : OP_SELL;
   int slippage = AdjustSlippage(Symbol(), Slippage);
   for (int i = OrdersTotal() - 1; i >= 0; --i)
   {
      if (OrderSelect(i, SELECT_BY_POS, MODE_TRADES))
      {
         if (OrderMagicNumber() == Magic && OrderSymbol() == Symbol() && OrderType() == order_type)
         {
            int ticket;
            do
            {
               ticket = OrderClose(OrderTicket(), OrderLots(), OrderClosePrice(), slippage);
               if (ticket == -1)
               {
                  ErrorHandling("CloseOrder", "Could not close order");
                  if (LastError != ErrorRetry)
                  {
                     return false;
                  }
               }
            } while (ticket == -1);
         }
      }
   }
   return true;
}

bool ExecuteOrder(Order* order)
{
   int type = order.GetOrder();
   switch (type)
   {
      case OrderLong:
         if (!CloseOrder(false)) return false;
         if (!OpenOrder(true)) return false;
         break;
      case OrderShort:
         if (!CloseOrder(true)) return false;
         if (!OpenOrder(false)) return false;
         break;
      case OrderLongClose:
         if (!CloseOrder(true)) return false;
         break;
      case OrderShortClose:
         if (!CloseOrder(false)) return false;
         break;
   }
   return true;
}

void SocketClose()
{
   delete Socket;
   Socket = NULL;
}

void OnInit()
{
   Socket = new ClientSocket(Host, Port);
   EventSetMillisecondTimer(TimerInterval);
}

void OnDeinit(const int reason)
{
   SocketClose();
   EventKillTimer();
}

void OnTimer()
{
   if (Socket)
   {
      string message;
      do
      {
         message = Socket.Receive("\n");
         if (message != "")
         {
            string alert = "";
            int order = OrderUnknown;
            
            string column[];
            int count = StringSplit(message, ',', column);
            for (int i = 0; i < count; ++i)
            {
               string model[];
               if (StringSplit(column[i], '=', model) == 2)
               {
                  StringReplace(model[0], " " , "");
                  StringReplace(model[1], " " , "");
                  if (model[0] == "alert")
                  {
                     alert = model[1];
                  }
                  else if (model[0] == "order")
                  {
                     order = StrToOrderType(model[1]);
                  }
               }
            }
            if (alert == AlertName)
            {
               OrderQueue.Add(new Order(order));
               int over = OrderQueue.Total() - OrderCountMax;
               for (int i = 0; i < over; ++i)
               {
                  OrderQueue.Delete(0);
               }
            }
         }
      } while (message != "");
      
      if (!Socket.IsSocketConnected())
      {
         SocketClose();
         Print("Client disconnected. Will retry.");
      }
   }
   
   if (WaitTimer.Update())
   {
      return;
   }
   
   while (OrderQueue.Total() > 0)
   {
      if (ExecuteOrder((Order*)OrderQueue.At(0)))
      {
         OrderQueue.Delete(0);
      }
      else
      {
         if (LastError == ErrorFatal)
         {
            ExpertRemove();
            break;
         }
         if (LastError == ErrorTurnNext)
         {
            WaitTimer.Set(TurnNextInterval);
            break;
         }
         if (LastError == ErrorPauseTurnNext)
         {
            WaitTimer.Set(PauseTurnNextInterval);
            break;
         }
      }
   }
}