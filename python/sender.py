import json
import socket
import threading
from datetime import datetime, timezone, timedelta

# -------------------------------
# AlertSender Class
# -------------------------------
class AlertSender:

    def __init__(self, host, port):
        self.host = host
        self.port = port
        self.connection_list = []
        self.timezone = timezone(timedelta(hours=9))

    def run(self):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.bind((self.host, self.port))
        sock.listen(5)
        while True:
            connection, address = sock.accept()
            th = threading.Thread(target=self.__client_handler, args=(connection, address), daemon=True)
            th.start()

    def send_message(self, message):
        data = message + '\n'
        for connection in self.connection_list[:]:
            try:
                connection.send(data.encode())
            except ConnectionResetError:
                connection.close()
                self.connection_list.remove(connection)
        print('{0} {1}'.format(message, datetime.now(self.timezone)))

    def __client_handler(self, connection, address):
        print('connection : {0} {1}'.format(address[0], address[1]))
        self.connection_list.append(connection)
        try:
            while True:
                request = connection.recv(1024)
                if not request:
                    break
        except Exception as e:
            print(e)
        connection.close()
        self.connection_list.remove(connection)
        print('disconnection : {0} {1}'.format(address[0], address[1]))

# -------------------------------
# EOF
# -------------------------------