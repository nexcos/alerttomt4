@echo off

set BUILD_SRC_NAME=server
call pyinstaller %BUILD_SRC_NAME%.py --onefile --hidden-import=pkg_resources.py2_warn

move /y dist\%BUILD_SRC_NAME%.exe %BUILD_SRC_NAME%.exe > nul

rd /s /q build
rd /s /q dist
rd /s /q __pycache__
del %BUILD_SRC_NAME%.spec

pause
exit