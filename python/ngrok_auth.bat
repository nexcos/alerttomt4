@echo off
setlocal enabledelayedexpansion

call :GET_INI ngrok TOKEN NGROK_TOKEN setting.ini
call ngrok.exe authtoken %NGROK_TOKEN%

pause
exit

rem ====================================================================
rem INIファイルから項目を読み込む
rem %1:セクション名 %2:キー名 %3 取得変数名 %4:INIファイル名
rem ====================================================================
:GET_INI
set RESULT=
set SN=
for /f "usebackq eol=# tokens=1,2 delims== " %%a in (%4) do (
   set V=%%a&set P=!V:~0,1!!V:~-1,1!&set S=!V:~1,-1!
   if "!P!"=="[]" set SN=!S!
   if "!SN!"=="%~1" if "!V!"=="%~2" (
      set RESULT=%%b
      goto GET_INI_EXIT
   )
)
set RESULT=ERR
:GET_INI_EXIT
set %3=%RESULT%
exit /b
rem ====================================================================