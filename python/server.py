import os
import sys
import threading
import configparser
from flask import Flask, request, abort
from sender import AlertSender

app = Flask(__name__)

@app.route('/')
def root():
    return 'online'

@app.route('/webhook', methods=['POST'])
def webhook():
    if request.method == 'POST':
        sender.send_message(request.get_data(as_text=True))
        return '', 200
    else:
        abort(400)

if __name__ == '__main__':
    
    dir_path = os.path.dirname(os.path.abspath(sys.argv[0]))
    file_path = os.path.join(dir_path, 'setting.ini')
    config = configparser.ConfigParser()
    config.read(file_path, 'utf_8_sig')
    socket_host = config.get('socket', 'HOST')
    socket_port = config.getint('socket', 'PORT')
    webhook_host = config.get('webhook', 'HOST')
    webhook_port = config.getint('webhook', 'PORT')

    sender = AlertSender(socket_host, socket_port)
    th = threading.Thread(target=sender.run, daemon=True)
    th.start()
    app.run(host=webhook_host, port=webhook_port)